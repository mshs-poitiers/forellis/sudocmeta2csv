# SudocMeta2Csv

Il est souvent très pratique d'utiliser un tableur pour réaliser des vérifications de saisies. 
Ces mêmes tableurs sont également très fréquent pour disséminer des données dans un Omeka par exemple.

Pour ces raisons, il nous est apparu utile de pouvoir collecter simplement toutes les métadonnées d'un lot de notices SUDOC identifiées par leur PPN et de les sauvegarder dans un tableur

Dans ce notebook Jupyter, nous rendons possible ce type de collecte sans que vous ayez à installer un quelconque logiciel. Il vous suffit de produire une liste de PPN.

https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fforellis%2Fsudocmeta2csv/9cab063f46907520fc783dc9043fc1150a3f96fc?filepath=sudocMetaToCsv.ipynb